import React from "react";
import Hero from "../../components/Hero";
import { Button } from "react-bootstrap";

function index() {
  const title = "How To Play the Game?";
  return (
    <div>
      <Hero title={title} />
      <p className="container text-center mt-5" style={{ fontSize: "20px" }}>
        The object of Tic Tac Toe is to get three in a row. You play on a three
        by three game board. The first player is known as X and the second is O.
        Players alternate placing Xs and Os on the game board until either
        oppent has three in a row or all nine squares are filled. X always goes
        first, and in the event that no one has three in a row, the stalemate is
        called a cat game.
      </p>
      <Button href="#game" className="d-flex justify-content-center">
        Let's Play
      </Button>
    </div>
  );
}

export default index;
