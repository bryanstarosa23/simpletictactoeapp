import React, { useState } from "react";
import Square from "./Square";
import Swal from "sweetalert2";

function Board() {
  const [boardSquares, setBoardSquares] = useState(Array(9).fill(null));
  const [xIsNext, setXIsNext] = useState(true);
  const [x, setX] = useState(0);
  const [o, setO] = useState(0);

  const handleClick = index => {
    const squares = [...boardSquares];
    if (calculateWinner(boardSquares) || squares[index]) return;
    squares[index] = xIsNext ? "X" : "O";
    setBoardSquares(squares);
    setXIsNext(!xIsNext);
  };

  const renderSquare = index => {
    return (
      <Square value={boardSquares[index]} onClick={() => handleClick(index)} />
    );
  };

  let status;
  const winner = calculateWinner(boardSquares);
  if (winner == null) {
    status = `Next Player: ${xIsNext ? "X" : "O"}`;
  } else {
    if (winner == "X") {
      console.log(winner);
      setX(x + 1);
      notice();
    } else if (winner == "O") {
      console.log(winner);
      setO(o + 1);
      notice();
    } else {
    }
  }

  const result = boardSquares.filter(word => word == null);
  if (result.length == 0 && winner == null) {
    Swal.fire("DRAW");
    setBoardSquares(Array(9).fill(null));
    setXIsNext(xIsNext ? "X" : "O");
  }

  function notice() {
    Swal.fire({
      title: `Player "${winner}" is the winner`,
      width: 600,
      padding: "3em",
      background: "#fff url(/images/trees.png)",
      backdrop: `
        rgba(0,0,123,0.4)
        url("/images/nyan-cat.gif")
        left top
        no-repeat
      `
    });
    setBoardSquares(Array(9).fill(null));
    setXIsNext(xIsNext ? "X" : "O");
  }

  function calculateWinner(squares) {
    const winningLines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
      [0, 3, 6]
    ];
    for (let i = 0; i < winningLines.length; i++) {
      const [a, b, c] = winningLines[i];
      if (squares[a] && squares[a] === squares[b] && squares[b] === squares[c])
        return squares[a];
    }
  }

  return (
    <>
      <div>
        <div className="status">{status}</div>
        <div>
          {renderSquare(0)}
          {renderSquare(1)}
          {renderSquare(2)}
        </div>
        <div>
          {renderSquare(3)}
          {renderSquare(4)}
          {renderSquare(5)}
        </div>
        <div>
          {renderSquare(6)}
          {renderSquare(7)}
          {renderSquare(8)}
        </div>
        <div className="status">
          <span className="mx-2">Player X: {x} </span>
          <span className="mx-2">Player O: {o} </span>
        </div>
      </div>
    </>
  );
}

export default Board;
