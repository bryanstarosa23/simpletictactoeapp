import React from "react";
import "./style.css";
import Board from "./Board";
import Hero from "../../components/Hero";

function TicTacToe() {
  const title = "Tic-Tac-Toe";
  return (
    <>
      <Hero title={title} />
      <div className="d-flex flex-column justify-content-center align-items-center mt-3">
        <div>
          <Board />
        </div>
      </div>
    </>
  );
}

export default TicTacToe;
