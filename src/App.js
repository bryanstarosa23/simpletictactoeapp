import React from "react";
import "./App.css";
import Header from "./components/Header";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import { css } from "@emotion/core";
import { PropagateLoader } from "react-spinners";

const loading = () => {
  return (
    <div className="bg-white vh-100 d-flex justify-content-center align-items-center">
      <PropagateLoader
        css={override}
        sizeUnit={"px"}
        size={15}
        color={"#FFAF06"}
        loading={true}
      />
    </div>
  );
};

const override = css`
  display: block;
  margin: 0 auto;
  border-color: white;
`;

const Game = React.lazy(() => import("./containers/Game/TicTacToe"));
const Instructions = React.lazy(() =>
  import("./containers/Instructions/index")
);

function App() {
  return (
    <Router>
      <React.Suspense fallback={loading()}>
        <Switch>
          <div className="App">
            <Header />
            <Route
              path="/Game"
              exact
              name="Game"
              render={props => <Game {...props} />}
            />
            <Route
              path="/"
              exact
              name="Instructions"
              render={props => <Instructions {...props} />}
            />
          </div>
        </Switch>
      </React.Suspense>
    </Router>
  );
}

export default App;
