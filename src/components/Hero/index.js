import React from "react";
import "./style.css";
import Card from "../UI/Card";

/**
 * @author
 * @function Hero
 **/

const Hero = props => {
  return (
    <div>
      <Card>
        <div style={{ padding: "50px 0" }} className="logo">
          {props.title}
        </div>
      </Card>
    </div>
  );
};

export default Hero;
