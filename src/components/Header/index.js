import React from "react";
import { NavLink } from "react-router-dom";
import "./style.css";

const Header = props => {
  return (
    <header className="header">
      <nav className="headerMenu">
        <NavLink to="/">Instructions</NavLink>
        <NavLink to="/game">Let's Play</NavLink>
      </nav>
    </header>
  );
};

export default Header;
